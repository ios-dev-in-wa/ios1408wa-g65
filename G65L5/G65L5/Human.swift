//
//  Human.swift
//  G65L5
//
//  Created by Ivan Vasilevich on 8/28/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class Human: NSObject {

	enum Gender: Int {
		case girl = 19
		case boy = 2
		case unknown = 88
	}
	
	private let averageGrowFactor = 2 //mm
	
	var age = 0
	var weight = 3000
	var height = 500
	let sex: Gender
	override var description: String {
		let result = "Human age of \(age) years, weight of \(weight/1000) kg"
		return result
	}
	
	
	init(gender: Gender) {
		sex = gender
	}
	
	
	
	func eat(foodWeight: Int) {
		weight += foodWeight
	}
	
	func sleep() {
		height += averageGrowFactor
	}

}
