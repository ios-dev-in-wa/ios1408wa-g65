//
//  Downloader.swift
//  G63L11
//
//  Created by Ivan Vasilevich on 7/7/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Downloader: NSObject {
	
	typealias CompletionImageBlock = (_ result: UIImage?, _ error: Error?) -> Void
	
	func downloadImage(url: URL, completion: CompletionImageBlock) {
			log()
			let linkStr = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/06/29051704/2018-Players-Classic-VW-Caddy-TFSI-for-Speedhunters-by-Paddy-McGrath-5.jpg"
			let url = URL(string: linkStr)!
			do {
				log()
				let data = try Data.init(contentsOf: url)
				log()
				let image = UIImage.init(data: data)
				completion(image, nil)
			}
			catch {
				print(error)
				completion(nil, error)
			}
		}
	
	func sendWeatherRequest() {
		/**
		weather
		get http://api.openweathermap.org/data/2.5/forecast
		*/
		
		// Add URL parameters
		let urlParams = [
			"q":"KIEV,uA",
			"mode":"{}",
			"APPID":"5a1afe5c9f21ea1636c623fdb880e0bb",
			]
		
		// Fetch Request
//		Alamofire.request("http://api.openweathermap.org/data/2.5/forecast", method: .get, parameters: urlParams)
//			.validate(statusCode: 200..<300)
//			.responseJSON { response in
//				if (response.result.error == nil) {
//
////					debugPrint("HTTP Response Body: \(response.data)")
//					let json = try! JSON(data: response.data!)
//					if var temp = json["list"][0]["main"]["temp"].double {
//						//Now you got your value
//						temp -= 273
//						print("kiev temp = \(temp)")
//					}
//				}
//				else {
//					debugPrint("HTTP Request failed: \(response.result.error)")
//				}
//		}
		
		Alamofire.request("http://api.openweathermap.org/data/2.5/forecast", method: .get, parameters: urlParams)
			.responseObject { (response: DataResponse<WeatherResponse>) in
				print("fdfdfdfd")
				print(" \(response.result.value?.city?.name ?? "no value") \(response.result.value?.list?.first?.conditions  ?? "no value") \(response.result.value?.list?.first?.temperature ?? 0)")
		}
		
		
	}
	
	func send1WeatherRequest() {
		/**
		weather
		get http://api.openweathermap.org/data/2.5/forecast
		*/
		
		// Add URL parameters
		let urlParams = [
			"q":"KIEV,uA",
			"mode":"{}",
			"APPID":"5a1afe5c9f21ea1636c623fdb880e0bb",
			]
		
		// Fetch Request
		Alamofire.request("http://api.openweathermap.org/data/2.5/forecast", method: .get, parameters: urlParams)
			.validate(statusCode: 200..<300)
			.responseJSON { response in
				if (response.result.error == nil) {
					debugPrint("HTTP Response Body: \(response.data)")
				}
				else {
					debugPrint("HTTP Request failed: \(response.result.error)")
				}
		}
	}
	
	



}
